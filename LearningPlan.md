# Software Engineering Report: 
# Degree: BSc Management with IT 

## Abstract

This report explores a dissection of my University BSc Management with Information Technology course. A Product Breakdown Structure (PBS), composing of the different compulsory and optional modules, as well as the dependencies and milestones, embodies the breakdown. A Gantt chart is also referred to. A discussion is then focused on the benefits against the drawbacks/risks of the software chosen to create the PBS and Gantt chart. The report finally ceases with a reflective section, with rumination about my learning in relation to the concepts of software engineering. 

## Introduction 

As part of my 4-year University degree course, BSc Management with Information Technology, I was required to produce a report for my software engineering module. The reports purpose was to produce a Product Breakdown Structure (PBS) of my BSc degree course, as well as creating a Gantt chart of the degree dissection to show when each module begins and ends, the time scales and any potential overlaps. Definitions of the two diagrams/charts are as follows:

- A Product Breakdown structure, as cited from productbreakdownstructure.com is: “an effective tool that details the physical components of a particular product, or system, under consideration. The formal PBS comes in the form of a hierarchy. It begins with the final product at the top of the hierarchy followed by the sub-categorized elements of the product.” Furthermore, the breakdown provides a visual representation of a product’s components and their relationships; as a result we are able to clearly see what the end product requires.

- A Gantt chart represents the total time span of a project, broken down into increments (for example: days, weeks, or months), as well as tasks that make up a project. “Frequently used in project management, a Gantt chart provides a graphical illustration of a schedule that helps to plan, coordinate, and track specific tasks in a project.”

With consideration to the nature of the report, and the fact that I am not breaking down a physical product or project but instead a degree course – my PBS and Gantt chart will not be traditional of those we may typically see. Although my PBS will have the same hierarchical layout, the contents as aforementioned will not relate to a product (but in this case a degree course), that being said, it will be similar in terms of achieving an ‘end goal’, and showing a breakdown in order to achieve that end result. 

In order to create the PBS, I initially had to research the modules and module descriptions for the 4-year course as well as the programme specification itself. This information was what I included in the diagram, for example the module codes ‘MM290 Managing Operations and Processes’, the number of credits and what the final assessments consisted of. Some of the information however I was not able to find, for example the optional modules for years 2,3 and 4. These are subject to change and therefore not officially published. This also meant that my information for year 4 was sparse as it only includes optional modules. The dependencies for my degree in BSc Management and IT are achieving a specific threshold and certain module percentages. For example: “IN ORDER TO PROGRESS FROM YEAR 1 TO YEAR 2 A STUDENT MUST ACHIEVE A THRESHOLD PERFORMANCE AND: (I) OBTAIN AT LEAST 40% IN 100 CREDITS (II) ACHIEVE NOT LESS THAN 35% IN THE REMAINING 20 CREDITS”. So, my performance directly affects my progression through the course. 

Furthermore, my Gantt chart created for showing the module dissections against time is expected to diverge a lot more. For example, for my purposes there will not be any need for typical Gantt table headings such as “completion %”, “cost” or even “priority”. Something we could normally see. These headings are not necessarily relevant to my specific chart; it would be hard to pinpoint exactly how much of a ‘cost’ each module was or how ‘completed’ it was for example. Moreover, all the modules on my degree count towards passing onto year 2, therefore there is neither a need for a ‘priority’ column. I have however included other typical column headings such as ‘Start Date’, ‘End Date’, ‘Duration’, ‘Predecessors’ and ‘Work’.

To help create my Gantt chart I used information from my PBS, specifically the module names and module codes that I had previously researched. I chose not include information about the year 1 module that I did not choose, as I was trying to make the chart personal to my degree. I then used the University of Reading course module site for current 2017/18 modules, to research the contact hours (lectures, practical classes, workshops, guided independent study), which I then inputted into the ‘Work (hrs)’ column. I roughly estimated (≈) the total contact hours for year 1 and 2, and was unable to estimate years 3 and 4 as I do not know the modules/work hours. I also used an equation to calculate the duration in days ‘=End Date-Start Date’, e.g. ‘=D2-C2’, and Excel’s formatting then allowed me to copy this equation to the whole column. In terms of predecessors, I could only think of having to pass the module assessments in each year; hence I have written years 1-3 in this column. 

This report will continue on to discuss the thought processes of choosing appropriate software for the PBS and Gantt chart, describing the benefits and drawbacks of different programmes, to then round off with a reflective conclusion. This is specifically to reflect upon what I have learned in relation to technical aspects and analysing my learning plan to concepts of software engineering; whilst also mentioning new approaches to changes of environment and handling the uncertainty involved. 

## Background 

Before creating the Product Breakdown Structure (PBS) and Gantt chart I had to consider several different software.

For my PBS I firstly contemplated using Microsoft PowerPoint for Mac. Seeing as its purpose is to fundamentally ‘present’, I thought that its slide layout would be ideal to clearly exhibit the PBS. Moreover, another benefit to using PowerPoint is that this software comes installed with the appropriate tools needed, for instance in the ‘SmartArt’ tab there are options to create ‘SmartArt Graphic’, which include tables and diagrams such as the hierarchical flow-table needed for the PBS. PowerPoint is also very easy to use and simple to navigate, and I am also familiar with this software making it ideal. However, a drawback to this software is that realistically the nature of the programme would make it very time consuming to create a PBS. I would have had to build the diagram step-by-step myself, using shape tools to create the process shapes along with drawing the connections to each process.

I also considered using software called MindView from matchware.com, a programme I found advertised on the site productbreakdownstructure.com. This specialised programme is for professional use, and allows users to easily create a range of sophisticated and neat tables, diagrams and charts from lesson plans to Gantt charts and firm SWOT analysis diagrams. I chose the ‘top down’ diagram, which I thought best represented a PBS, and after exploring the software I discovered that I could easily build a PBS through the use of two simple command buttons ‘Branch’ and ‘Sub-Branch’. This simple tool would make building the PBS quicker than if I were to use Microsoft PowerPoint, a major advantage. Furthermore, another benefit to MindView is that it allowed me to change the layout of my diagram including width, length, font size, styles and colours at the click of a button. Allowing me to personalise, alter and thus perfect my PBS diagram without the hassle. Nevertheless, a drawback to the MindView programme is the time it takes to install and learn how to navigate. This is time consuming, as matchware.com asks for your details and then you must go through a lengthy installation process. Additionally, the free 30-day trial comes with a users guide, yet this 150 page + guide just highlights how sophisticated this software can be; somewhat unsurprising due to its professional and credible nature. Despite this hindrance, the benefits of this software outweigh its drawbacks making it the ideal programme for my PBS over Microsoft PowerPoint.

In terms of creating my Gantt chart, I considered using the MindView software, Microsoft Excel (for Mac) and gantter.com. Initially, I speculated that using MindView would result as a huge timesaving benefit as the data could be directly transferred from my PBS into the Gantt chart at the click of a button. Moreover, I had quickly become familiar with this software after playing around with it when creating the PBS, and as a result this made me disregard using and signing up for gantter.com, an online site dedicated to making Gantt charts. Having to learn to navigate new software is a timewasting drawback, and a risk if it does not deliver specifically what I am looking for. As aforementioned, using MindView’s hassle-free Gantt chart is a benefit. However, an important factor to recognise is that Gantt charts were originally ‘developed as a production control tool [,] [f]requently used in project management’. Therefore, considering that my chart will include data from my BSc Management with IT course, it is expected to differ from what we may traditionally see. Consequently, the benefits of MindView were outweighed by the drawback that after tests I found the software was not flexible enough. Its layout was suited for its use as a ‘production control tool’, and the software restricted any-sort of adaptation from its traditional layout. If it was adaptable, then I did not have enough experience with the software to be able to override this issue. 

For this reason, I decided to consider the benefits and drawbacks of using Microsoft’s Excel. Excel is not a software designed for creating Gantt charts, the 2011 version I have as well as later updates do not have pre-installed Gantt templates. I found a Microsoft template for a Gantt chart on their online-site however like MindView, it was restrictive. Thus, after carrying out further research on the internet I found an online guide called: ‘How to make a Gantt chart in Excel 2010, 2013 and Excel 2016’ by Svetlana Cheusheva on Ablebits.com. Having the 2011 Excel I would have to adapt the instructions, but luckily the versions only differ slightly. Furthermore, through using this guide I found Microsoft’s Excel could be beneficial software, ideal for the needs of my Gantt chart. The fact that Excel does not have a flexible Gantt chart template is outweighed by the fact I discovered (through the guide) that I could manipulate the formatting of the stacked 2D bar chart to mimic one. Not to mention, Excel is ideal for when wanting to calculate the ‘Duration’ of each task (module), I can simply copy a formula down an entire column in one mouse-drag. 

## Reflective

This report required me to research around PBS’ and Gantt charts in order to produce ones appropriate for the breakdown of my University degree. Initially the nature of the task was difficult to comprehend, as visually I did not know what either a Product Breakdown Structure or a Gantt chart looked like. However after grasping enough understanding of these, I was able to piece together what potential information and data I needed to produce them. Following on from this I started considering software (as aforementioned), and thus after I understood the task and evaluated the appropriate programmes; I was able to successfully and quite easily produce the PBS and Gantt charts. The task I thought was hard to assimilate initially, sounding complicated, but after research and thought I found it easier. 
In terms of technical aspects, I have learnt that Product Breakdown Structures are used to separate the steps needed to achieve an end goal. In relation to software engineering specifically, these can be helpful when developing new software. It is helpful to see a visual dissection of steps in order to analyse them and communicate them. For example if a software engineering firm were working on a particularly complex project, a PBS would be ideal to help breakdown the different elements needed (such as codes); and it would also highlight the dependencies and milestones of the project. 
Moreover, I have also learnt the purpose of Gantt charts, and in relation to software engineering these would be useful (like PBS’), to highlight the dependencies and milestones as identified in the predecessors column. Especially when trying to complete a project, being able to quickly see how far along a project was or the task priorities would be helpful, notably more so alongside a PBS for reasons aforementioned. Furthermore, a Gantt chart would also benefit a software engineering firm to keep focused on deadlines. 
 “Deadlines that are set by engineering teams will largely be informed by estimating work”. 
According to Hochman Consultants, a marketing agency who also have web development capabilities: “During the average software development project 50% of time is spent debugging. With a website that means the software must be tested on all common browsers, including desktop, tablet, and mobile versions. All this testing takes time, [s]etting artificial deadlines on software projects tends to produce low quality work” as engineers are forced to rush.
That being said, it is still important to make estimated deadlines fairly accurate, especially if a software engineering firm is completing a project for a customer. Gantt charts force a firm to think ahead about start dates, end dates, priorities, work (hours) needed and percentage of completion. Therefore by forecasting ahead, deadlines are clearly laid out and data such as completion can help to make sure a project stays on track. Gantt charts work as clear visual guides, this isn’t to say a deadline cannot be moved; but at least they can be slightly more accurate.
Considering the formal uses of both the PBS and Gantt charts, I think I have successfully adapted my versions to suit my degree whilst also keeping within the basic requirements/characteristics of each. 
When examining my PBS, I noted how the structure of my University degree differs from the structure of my learning at sixth form. At sixth form I took all exam-assessed subjects, therefore my A levels were not dependent on achieving a certain percentage in coursework for example. Nor did tests count towards a final grade; these were usually just mock exams in preparation for the real ones. Consequently, I did not have as many dependencies during my A levels as I do at University. What I did have however- similar to my University degree- was a certain minimum achievement grade to progress from AS to A level. These were only assessed at the end of the summer term however. That having been said, we could consider revision as a dependency. I was dependent on my revision throughout the year to both pass into my final year and to achieve A levels. 
When reflecting on the difficulties I faced when completing the task, I think it was my lack of initial knowledge that held me back the most. For example, although it would have been quicker to use MindView for my Gantt chart, I did not have enough knowledge or experience with the software to be able to use it to my advantage. I thought it would be quicker to use a more familiar programme (Excel) rather than to spend time scrolling through MindView’s vast user guide. If I were more comfortable with this software then I most definitely would have spent less time on producing the chart. Furthermore, I found it was useful to use the Internet to research articles and guides when faced with difficulty. As referenced in my report for example, I used a guide from ablebits.com to teach me how to produce a Gantt chart on Excel after spending somewhat wasted time downloading unhelpful templates. It was mainly a mixture of trial and error and Internet research that helped me overcome complications. 

In terms of being unable to complete some elements of my PBS and Gantt chart, particularly the optional modules of years 2, 3 and 4, it would have been helpful to be able to access this information in order to increase accuracy. I could have perhaps asked those in years 2, 3 and 4 what modules were available to them on my degree course or even email a lecturer. However, it would not have been guaranteed that the modules available would not have changed; it would have not made my PBS or Gantt chart 100% accurate due to the module uncertainty. 













## Reference list

PDF

MindView for Mac User Guide (2017) (Accessed 02/12/17)

University of Reading, (2017) PROGRAMME SPECIFICATION, BSc Management with Information Technology
URL: http://www.reading.ac.uk/progspecs/pdf18/UFMANWINFTEC18.pdf (Accessed 02/12/17)

Software

gantter.com, (2017) Chart maker
URL: https://www.gantter.com (Accessed 02/12/17)

MatchWare, global leader in professional mind mapping and enterprise meeting management solutions, MindView mind mapping software, 30 day free trial for Mac
URL: https://www.matchware.com  (Installed: 02/12/17)

Microsoft 
Excel (2011) for Mac 
PowerPoint (2011) for Mac

Webpage

Gantt.com, (2017), What is a Gantt Chart?
URL: http://www.gantt.com (Accessed 02/12/17)

Jonathan Hochman, Hochman Consultants (2015), The Danger of Deadlines in Web Development
URL: https://www.hochmanconsultants.com/no-deadlines-for-web-development/ (Accessed: 07/12/17)
Margaret Rouse, WhatIs.com, Search Software Quality (updated 2007), Definition Gantt chart
URL: http://searchsoftwarequality.techtarget.com/definition/Gantt-chart (Accessed: 01/12/17)

Product BreakdownStructure, (2017) What is a Product Breakdown Structure? 
URL: http://www.productbreakdownstructure.com (Accessed: 01/12/17)

Ryan Sparetz, (2016) How should deadlines be used in software engineering?
URL: https://blog.keen.io/how-should-deadlines-be-used-in-software-engineering-9eb23d513e8d (Accessed: 07/12/17)

Svetlana Cheusheva, (updated 23/05/17) How to make a Gantt chart in Excel 2010, 2013 and Excel 2016, 
URL: https://www.ablebits.com/office-addins-blog/2014/05/23/make-gantt-chart-excel/  (Accessed: 02/12/17)

UEfAP, Genres in academic writing: Reflective writing
URL: https://www.uefap.com/writing/genre/reflect.htm (Accessed: 06/12/17)

University of Reading, (2017) About us, Term Dates, Session 2017/18, Session 2018/19, Session 2019/20
URL: http://www.reading.ac.uk/about.aspx (Accessed: 02/12/17)

University of Reading, (2017) Module Descriptions
Mathematical, Physical and Computational Sciences - Modules for 2017-18 
URL: https://www.reading.ac.uk/modules/module.aspx?sacyr=1718&school=MPS (Accessed: 02/12/17)
Henley Business School - Modules for 2017-18 
URL: https://www.reading.ac.uk/modules/module.aspx?sacyr=1718&school=HBS (Accessed: 02/12/17)

Link to my Gantt Chart table data
![Link to my Gantt Chart](d0102e2b5359f41660d51fdb60dec58f90a52bc5)

Link to my Gantt Chart
![Link to my Gantt Chart](5091c5e88fae827da7bc7189b1e2faac93d6a829)

Link to my PBS
![Link to my PBS](90f3cc03784f3b588ff281801c6327394332ad02)

Link to my CV
![Link to my CV](186b6b016a6e824cb97d6551528ff5815f401881)


